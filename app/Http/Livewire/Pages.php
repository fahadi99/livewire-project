<?php

namespace App\Http\Livewire;

use App\Models\Page;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Livewire\WithPagination;
use Illuminate\Validation\Rule;
use Livewire\WithFileUploads;
use DataTables;


class Pages extends Component
{
    use WithPagination;
    use WithFileUploads;

    public $modalFormVisible = false;
    public $modalConfirmDelete = false;
    public $modelId;
    public $name;
    public $description;
    public $image;
    public $status;
    public $slug;
    public $isDefaultHome;
    public $isDefault404;

    /**
     * validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => ['required', Rule::unique('pages', 'slug')->ignore($this->modelId)],
            'description' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048', // Add validation for image uploads
            'status' => 'required|in:active,inactive',
        ];
    }

    /**
     * the livewire mount function
     *
     * @return void
     */
    public function mount()
    {
        $this->resetPage();
    }

    /**
     * Show the form modal
     * of the create function.
     *
     * @return void
     */
    public function createShowModal()
    {
        $this->resetValidation();
        $this->reset();
        $this->modalFormVisible = true;
    }

    /**
     * Show the form modal
     * of the update function
     *
     * @param  mixed $id
     * @return void
     */
    public function updateShowModal($id)
    {
        $this->resetValidation();
        $this->reset();

        $this->modelId = $id;
        $data = Page::find($this->modelId);
        $this->modalFormVisible = true;
        $this->name = $data->name;
        $this->slug = $data->slug;
        $this->description = $data->description;
        $this->image = $data->image;
        $this->status = $data->status;
        $this->isDefaultHome = $data->is_default_home;
        $this->isDefault404 = $data->is_default_404;
    }

    /**
     * Show the modal of
     * the delete function
     *
     * @return void
     */
    public function deleteShowModal($id)
    {
        $this->modelId = $id;
        $this->modalConfirmDelete = true;
    }

    /**
     * runs when title value changes
     *
     * @param  mixed $value
     * @return void
     */
    public function updatedTitle($value)
    {
        $this->slug = Str::slug($value);
    }

    /**
     * runs when isDefaultHome value changes
     *
     * @return void
     */
    public function updatedIsDefaultHome()
    {
        $this->isDefault404 = null;
    }

    /**
     * runs when isDefaul404 value changes
     *
     * @return void
     */
    public function updatedIsDefault404()
    {
        $this->isDefaultHome = null;
    }

    /**
     * Unassign the default home page in the pages table
     *
     * @return void
     */
    public function unassignDefaultHomePage()
    {
        if ($this->isDefaultHome) {
            Page::defaultHome()->update([
                'is_default_home' => null
            ]);
        }
    }

    /**
     * Unassign the default 404 page in the pages table
     *
     * @return void
     */
    public function unassignDefault404Page()
    {
        if ($this->isDefault404) {
            Page::default404()->update([
                'is_default_404' => null
            ]);
        }
    }

    /**
     * get the model data
     *
     * @return void
     */
    public function modelData()
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'is_default_home' => $this->isDefaultHome,
            'is_default_404' => $this->isDefault404,
            'description' => $this->description,
            'image' => $this->image ? $this->image->store('public/images') : null,
            'status' => $this->status,
        ];
    }

    /**
     * create a new page
     *
     * @return void
     */
    public function create()
    {
        $this->validate();
        $this->unassignDefaultHomePage();
        $this->unassignDefault404Page();
        Page::create($this->modelData());
        $this->dispatchBrowserEvent('event-notification', [
        'eventName' => 'New Page :D',
        'eventMessage' => 'A new page has been created!'
    ]);
    $this->reset();
    }

    /**
     * read pages from database
     *
     * @return void
     */
    public function read()
    {
        return Page::orderBy('id', 'asc')->paginate(3);
    }

    /**
     * update an existing page
     *
     * @param  mixed $id
     * @return void
     */
    public function update()
    {
        $this->validate();
        $this->unassignDefaultHomePage();
        $this->unassignDefault404Page();
        $page = Page::find($this->modelId);
        $oldImage = $page->image;

        if ($this->image) {
            // Store the new image and update the image path
            $page->image = $this->image->store('public/images');
        }


       // Delete the old image if it has been updated
        if ($this->image && $oldImage) {
            Storage::delete($oldImage);
}
        elseif (!$this->image && $oldImage) {
    // If a new image was not uploaded, but there was an old image, retain the old image path
            $page->image = $oldImage;
}
                $page->update($this->modelData());


        $this->dispatchBrowserEvent('event-notification', [
            'eventName' => 'Updated Page :D',
            'eventMessage' => 'The page "' . $this->name . '" has been updated!'
        ]);
        $this->reset();
    }

    /**
     * delete a page
     *
     * @return void
     */
    public function delete()
    {
        $page = Page::find($this->modelId);
        $image = $page->image;
        $page->delete();

        // Delete the associated image
        if ($image) {
            Storage::delete($image);
        }

        $this->dispatchBrowserEvent('event-notification', [
            'eventName' => 'Page Deleted :,(',
            'eventMessage' => 'The page "' . $this->modelId . '" has been deleted!'
        ]);
        $this->reset();
    }

    /**
     * The livewire render function.
     *
     * @return void
     */
    public function render()
{
    return view('livewire.pages', [
        'pages' => $this->read(),
        'name' => $this->name,
    ]);
}
}
