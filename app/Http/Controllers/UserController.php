<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\User;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $users = User::select('*');

            return Datatables::of($users)
                ->addColumn('action', function ($user) {
                    $action = '<a href="javascript:void(0)" data-id="' . $user->id . '" class="edit btn btn-info btn-sm">Edit</a>';
                    $action .= '&nbsp;<a href="javascript:void(0)" data-id="' . $user->id . '" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $action;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.users');
    }

    public function store(Request $request)
    {
        // Validation logic here

        $user = new User([
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'phone' => $request->phone,
            'role' => $request->role,
            'email' => $request->email,
            'status' => $request->status,
            'created_by' => auth()->user()->id,
            'updated_by' => auth()->user()->id,
        ]);

        $user->save();

        return response()->json(['success' => 'User created successfully']);
    }

    public function edit($id)
    {
        $user = User::find($id);
        return response()->json($user);
    }

    public function update(Request $request, $id)
    {
        // Validation logic here

        $user = User::find($id);
        $user->update([
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'email' => $request->email,
            'phone' => $request->phone,
            'status' => $request->status,
            'role' => $request->role,
            'updated_by' => auth()->user()->id,
        ]);

        return response()->json(['success' => 'User updated successfully']);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return response()->json(['success' => 'User deleted successfully']);
    }
}
