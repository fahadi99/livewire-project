<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\PageDataTable;
use App\Http\Livewire\Pages;
use App\Models\Page;
use Yajra\DataTables\DataTables;


class PageController extends Controller
{
    public function index(Request $request)
{
    if ($request->ajax()) {
        $data = Page::select('*');
        return DataTables::of($data)
            ->addColumn('actions', function ($page) {
                return view('livewire.pages', compact('page'));
            })
            ->make(true);
    }

    return view('admin.pages');
}

public function datatable()
{
    return DataTables::of(Page::query())
    ->addColumn('actions', function ($page) {
        return view('livewire.pages', compact('page'));
    })
    ->make(true);
}

}
