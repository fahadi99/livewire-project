<a href="{{ route('navigation-menus.edit', $id) }}" class="btn btn-primary">Edit</a>
<button class="btn btn-danger" onclick="deleteNavigationMenu({{ $id }})">Delete</button>

<script>

function deleteNavigationMenu(id) {
    if (confirm('Are you sure you want to delete this item?')) {
        // Implement your delete logic here
    }
}
</script>
