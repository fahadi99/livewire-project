<div class="p-6">
    <div class="flex justify-end px-4 py-3 text-right items-centre sm:py-6">
        <x-jet-button wire:click="createShowModal">
            {{ __('Create') }}
        </x-jet-button>
    </div>
    {{-- The data table --}}

    <div class="flex flex-col">
        <div class="my-2 overflow-x-auto sm:mx-6 lg:mx-8">
            <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
                <div class="overflow-hidden border-b border-gray-200 shadow sm:rounded-lg">
                    <table id="page-table" class="table" style="width: 100%;">
                        <thead>
                            <tr>
                                <th class="table-head">ID</th>
                                <th class="table-head">Name</th>
                                <th class="table-head">Link</th>
                                <th class="table-head">Description</th>
                                <th class="table-head">ImagePath</th>
                                <th class="table-head">Status</th>
                                <th class="table-head">Actions</th> <!-- Add Actions column -->
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                            @if ($pages->count())
                                @foreach ($pages as $page)
                                    <tr>
                                        <td class="table-data">
                                            {{ $page->id }}
                                        </td>
                                        <td class="table-data">
                                            {{ $page->name }}
                                            {!! $page->is_default_home ? '<span class="text-sm font-bold text-green-600">[Default Home Page]</span>' : '' !!}
                                            {!! $page->is_default_404 ? '<span class="text-sm font-bold text-red-600">[Default 404 Error Page]</span>' : '' !!}
                                        </td>

                                        <td class="table-data">
                                            <a href="{{ URL::to('/' . $page->slug) }}" target="_blank"
                                                class="text-indigo-600 hover:text-indigo-900">
                                                {{ $page->slug }}
                                            </a>
                                        </td>
                                        <td class="table-data">{!! \Illuminate\Support\Str::limit($page->description, 50, '...') !!}</td>
                                        </td>
                                        <td class="table-data">
                                            {{ $page->image }}
                                        </td>
                                        <td class="table-data">
                                            {{ $page->status }}
                                        </td>
                                        <td class="flex justify-end gap-2 table-data">
                                            <x-jet-button wire:click="updateShowModal({{ $page->id }})">
                                                {{ __('Edit') }}
                                            </x-jet-button>
                                            <x-jet-danger-button wire:click="deleteShowModal({{ $page->id }})">
                                                {{ __('Delete') }}
                                            </x-jet-danger-button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="table-data" colspan="5">No Results Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <br />
    {{ $pages->links() }}

    {{-- Modal Form --}}
    <x-jet-dialog-modal wire:model="modalFormVisible">
        <x-slot name="title">
            {{ __('Save Page') }} {{ $modelId }}
        </x-slot>

        <x-slot name="content">
            <div class="mt-4">
                <x-jet-label for="name" value="{{ __('Name') }}" />
                <x-jet-input id="name" class="block w-full mt-1" type="text" name="name"
                    wire:model.debounce.500ms="name" />
                @error('name')
                    <span class="error">{{ $message }}</span>
                @enderror
            </div>
            <div class="mt-4">
                <x-jet-label for="slug" value="{{ __('Slug') }}" />
                <div class="flex mt-1 rounded-md shadow-sm">
                    <span
                        class="inline-flex items-center px-3 py-3 text-sm text-gray-500 border border-r-0 border-gray-300 rounded-l-md bg-gray-50">
                        http://127.0.0.1:8000/
                    </span>
                    <input wire:model.lazy="slug"
                        class="flex-1 block w-full pl-1 transition duration-150 ease-in-out border rounded-none form-input rounded-r-md sm:text-sm sm:leading-5"
                        placeholder="url-slug">
                </div>
                @error('slug')
                    <span class="error">{{ $message }}</span>
                @enderror
            </div>
            <div class="mt-4">
                <label>
                    <input class="form-checkbox" type="checkbox" value="{{ $isDefaultHome }}"
                        wire:model="isDefaultHome">
                    <span class="ml-2 text-sm text-gray-600">Set as the default home page</span>
                </label>
            </div>
            <div class="mt-4">
                <label>
                    <input class="form-checkbox" type="checkbox" value="{{ $isDefault404 }}"
                        wire:model="isDefault404">
                    <span class="ml-2 text-sm text-red-600">Set as the default 404 error page</span>
                </label>
            </div>
            <div class="mt-4">
                <x-jet-label for="description" value="{{ __('Description') }}" />
                <div class="rounded-md shadow-sm">
                    <div class="mt-1 bg-white">
                        <div class="body-content" wire:ignore>
                            <trix-editor class="trix-content" x-ref="trix" wire:model.debounce.500ms="description"
                                wire:key="trix-content-unique-key"></trix-editor>
                        </div>
                    </div>
                @error('description')
                    <span class="error">{{ $message }}</span>
                @enderror
            </div>
            <div class="mt-4">
                <x-jet-label for="image" value="{{ __('Image') }}" />
                <input type="file" wire:model="image" id="image" class="block w-full mt-1 form-input">
                @error('image')
                    <span class="error">{{ $message }}</span>
                @enderror
            </div>
            <div class="mt-4">
                <x-jet-label for="status" value="{{ __('Status') }}" />
                <select id="status" wire:model="status" class="block w-full mt-1 form-select">
                    <option value="active">Active</option>
                    <option value="inactive">Inactive</option>
                </select>
                @error('status')
                    <span class="error">{{ $message }}</span>
                @enderror
            </div>
        </x-slot>

        <x-slot name="footer">
            <x-jet-secondary-button wire:click="$toggle('modalFormVisible')" wire:loading.attr="disabled">
                {{ __('Cancel') }}
            </x-jet-secondary-button>

            @if ($modelId)
                <x-jet-danger-button class="ml-3" wire:click="update" wire:loading.attr="disabled">
                    {{ __('Update') }}
                </x-jet-danger-button>
            @else
                <x-jet-danger-button class="ml-3" wire:click="create" wire:loading.attr="disabled">
                    {{ __('Create') }}
                </x-jet-danger-button>
            @endif
        </x-slot>
    </x-jet-dialog-modal>

    {{-- Delete Modal --}}
    <x-jet-dialog-modal wire:model="modalConfirmDelete">
        <x-slot name="title">
            {{ __('Delete Page') }}
        </x-slot>

        <x-slot name="content">
            {{ __('Are you sure you want to delete this page? Once the page is deleted, all of its resources and data will be permanently deleted.') }}
        </x-slot>

        <x-slot name="footer">
            <x-jet-secondary-button wire:click="$toggle('modalConfirmDelete')" wire:loading.attr="disabled">
                {{ __('Cancel') }}
            </x-jet-secondary-button>

            <x-jet-danger-button class="ml-3" wire:click="delete" wire:loading.attr="disabled">
                {{ __('Delete Page') }}
            </x-jet-danger-button>
        </x-slot>
    </x-jet-dialog-modal>

@push('scripts') <!-- Use 'push' to add scripts in your Blade layout -->
<script>
    window.livewire.debug = true;
</script>
<script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
{{-- <script src="https://cdn.datatables.net/buttons/2.4.2/js/dataTables.buttons.min.js"></script> --}}
<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>

<script src="{{ mix('js/app.js') }}"></script>
<script>
    document.addEventListener('livewire:load', function () {
        console.log('livewire loaded');
        if ($.fn.DataTable.isDataTable('#page-table')) {
            $('#page-table').DataTable().destroy();
        }

        $('#page-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('pages.datatable') }}",
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
            },
            columns: [
                { data: 'name', name: 'name' },
                { data: 'description', name: 'description' },
                { data: 'image', name: 'image' },
                { data: 'status', name: 'status' },
                { data: 'actions', name: 'actions', orderable: true, searchable: true }
            ]
        });
    });
</script>
@endpush
</div>

