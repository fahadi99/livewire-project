<?php

use App\Http\Livewire\Pages;
use App\Http\Livewire\FrontPage;
use app\DataTables\PageDataTable;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\UserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => [
    'auth:sanctum',
    'verified',
    'roleaccess',
]], function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::get('/pages/datatable', [PageController::class, 'index'])->name('pages.datatable');

    Route::get('/navigation-menus', function () {
        return view('admin.navigation-menus');
    })->name('navigation-menus');
    Route::view('/users', 'admin.users')
        ->name('users');
    Route::view('/user-permissions', 'admin.user-permissions')
        ->name('user-permissions');
        // Route::get('/pages/datatable', PageDataTable::class)->name('pages.datatable');
        // Route::get('/pages-index', [PageController::class, 'index'])->name('pages.index');
    Route::get('/users-datatable', [UserController::class, 'datatable'])->name('users.datatable');




});

Route::get('/{urlslug}', FrontPage::class);
Route::get('/', FrontPage::class);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
