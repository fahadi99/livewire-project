<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'fname' => 'Fahad Muhammad',
                'lname' => 'Khan',
                'phone' => '2314413452',
                'email' => 'admin@example.com',
                'role' => 'admin',
                'status' => 'active',
                'password' => '$2y$10$mciTph.QOp/uFbZuEUrj.u3Z5Z8WXwWYHeMAzDssHOivSeXc2ti6y',
            ]
        );
        User::create([
            'fname' => 'Ali',
            'lname' => 'Shehbaz',
            'email' => 'ali@example.com',
            'phone' => '12132135451',
            'role' => 'user',
            'status' => 'inactive',
            'password' => '$2y$10$B5zMDb5V5bMxqpAC3/APSOcNvOdZhoucWJh7LFFXmWuppEiu9Vb2W'
        ]);
    }
}
