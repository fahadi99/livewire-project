<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('name'); // Add Name field
            $table->longText('description')->nullable(); // Add Description field
            $table->string('image')->nullable(); // Add Image field
            $table->string('slug')->nullable();
            $table->string('status')->default('active'); // Add Status field with default value 'active'
            $table->timestamps(); // This will add Created_at and Updated_at
            $table->softDeletes(); // This will add Deleted_at

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
};
